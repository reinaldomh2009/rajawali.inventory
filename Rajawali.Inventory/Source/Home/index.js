﻿import Vue from 'vue';
import HomeComponent from './Home.vue';

new Vue({
    el: "#app",
    components: {
        HomeComponent
    }
})